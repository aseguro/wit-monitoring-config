FROM debian:sid

MAINTAINER "aseguro" <alfonso@wit.com>

RUN apt-get update && apt-get -y install curl gnupg ca-certificates

RUN echo 'deb https://mirrors.wit.com/debian sid main contrib non-free' > /etc/apt/sources.list && \
    echo 'deb https://mirrors.wit.com/debcore sid main' >> /etc/apt/sources.list && \
    curl https://mirrors.wit.com/debcore/public.key | apt-key add - && \
    apt-get update

RUN apt-get -y install devscripts build-essential lintian debhelper config-package-dev && apt-get clean
