#!/bin/bash

email='alfonso@wit.com'
expected='6'
sessions=`vtysh -c "show bgp summary" |grep fspine |wc -l`

if [ "$expected" -ne "$sessions" ]
then
    echo "There are just $sessions BGP sessions detected" | mail -s "BGP sessions error $HOSTNAME" $email
    exit 1
else
    exit 0
fi
