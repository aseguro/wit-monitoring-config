#!/bin/bash

email='alfonso@wit.com'
dev=`cat /proc/partitions |grep sd |awk '{print$4}'|grep -v [0-9]|sort`

for dev in $dev
do
    status=`/usr/sbin/smartctl -H /dev/$dev|grep "test result" |awk -F ":" '{gsub(/ /, "", $2);print$2}'`
        if [ "$status" != "PASSED" ]
        then
            echo "SMART reports DISK $dev $status" | mail -s "SMART error $HOSTNAME" $email
            exit 1
        else
            exit 0
        fi
done
