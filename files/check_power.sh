#!/bin/bash

power=`/usr/bin/ipmitool dcmi power reading|grep Instantaneous|grep Instantaneous |awk '{print $4}'`
output="/var/log/power.log"
date=`TZ='America/Los_Angeles' date`
retention=24

if [[ ! -e $output ]]; then
    touch $output
fi

#Limit log to last $retention entries

if [ `wc -l $output| awk '{print $1}'` -ge "$retention" ]; then
    sed -i 1d $output
fi

echo -n $power" ">> $output && echo $date>> $output
