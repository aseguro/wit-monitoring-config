#!/bin/bash

email='alfonso@wit.com'
ceph_health=$(ceph -s)
ceph_id=$(echo "$ceph_health" |grep "id:" |awk '{print$2}')
ceph_status=$(echo "$ceph_health" |grep health | sed -e 's/.*: //')

if [ "$ceph_status" != "HEALTH_OK" ]
then
    echo "$ceph_health" | mail -s "CEPH CLUSTER $ceph_id ISSUES" $email
    exit 1
else
    exit 0
fi

