#!/bin/bash

email='alfonso@wit.com'
process='stack-worker'
RESULT=`pgrep $process`

    if [ "${RESULT:-null}" = null ]
        then
            echo "${PROCESS} not running"|mail -s "${PROCESS} not running on $HOSTNAME" $email
	    exit 1
    else
            exit 0
fi
