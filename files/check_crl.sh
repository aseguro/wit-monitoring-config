#!/bin/bash

email='alfonso@wit.com'
WARNING_DAYS=28
FILE='/etc/ipsec.d/crls/ca-wit.crl'
warning_seconds=$(($WARNING_DAYS * 86400))

# validate CRLs
exp_date="`/usr/bin/openssl crl -nextupdate -noout -in $FILE | sed -e 's/.*=//'`"
exp_seconds="`date --date="$exp_date" +%s`"
now_seconds="`date +%s`"

# compare values
if [[ $exp_seconds > $(( now_seconds + warning_seconds )) ]]
then
    echo "CRL expiring soon: $FILE will expire on $exp_date" | mail -s "CRL warning $HOSTNAME"
    exit 1
else
    exit 0
fi
