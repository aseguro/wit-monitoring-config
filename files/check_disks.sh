#/bin/bash

email='alfonso@wit.com'
installed_disks='12'
detected_disks=`/usr/local/bin/sas3ircu 0 display|grep "Device is a Hard disk" |wc -l`

if [ "$installed_disks" -ne "$detected_disks" ]
then
    echo "There are just $detected_disks disks detected, this system should have $installed_disks disks" | mail -s "DISK MISSING $HOSTNAME" $email
    exit 1
else
    exit 0
fi
